class Customer {
	constructor (email,cart,orders){
		this.email = email
		this.cart = new Cart()
		this.orders = []
	}

	checkOut(){
		if (this.cart.contents.length > 0) {
			this.orders.push({
				product: this.cart.contents,
				totalAmount: this.cart.computeTotal().totalAmount
			})
		}
	}
}

class Cart {
	constructor(){
		this.contents = []
		this.totalAmount = 0
	}

	addToCart(product,quantity){
		this.contents.push({
			product: product,
			quantity: quantity
		})

		return this
	}

	showCartContents(){
		console.log(this.contents)

		return this
	}

	updateProductQuantity(name, quantity){
		this.contents.find(item => item.product.name === name).quantity = quantity

		return this
	}

	clearCartContents(){
		this.contents = []

		return this
	}

	computeTotal(){
		this.totalAmount = 0

		if(this.contents.length > 0){
			this.contents.forEach(item => {
				this.totalAmount = this.totalAmount + (item.product.price * item.quantity)
			})
		}
	}
}

class Product {
	constructor(name,price){
		this.name = name
		this.price = name 
		this.isActive = true
	}

	archive(){
		if(this.isActive === true){
			this.isActive = false
		}

		return this
	}

	updatePrice(newPrice){
		this.price = newPrice

		return this
	}

}